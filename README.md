# IBS 574 - Computational Biology & Bioinformatics, Spring 2018, Emory University

# RNA-Seq Exercise: raw reads to differential expression [^1]

In this exercise we will go through all of the steps of a simple RNA-Seq differential expression analysis using High Performance Computing (HPC), which will include:

## 1. Connect HPC Server via SSH [^2] [^3] [^4]
SSH allows you to connect to Human Genetics Compute Cluster (HGCC) server securely and perform linux command-line operations.

```
ssh <user_name>@hgcc.genetics.emory.edu
```
Alternatively, Open a web browser (Safari or Google Chrome) and enter the following URL into your browser address bar, then press.

```
https://hgcc.genetics.emory.edu:22443
```

## 2. Create project directory and sub-directories
From your home directory i.e /home/<user_name>

```bash
mkdir -p rnaseq/{data,script,out,logs}
```

## 3. Copy raw data into ~/rnaseq/data and extract .tar.gz file
I have placed test dataset at **/scratch** directory. 

```bash
cp -v /scratch/ibs_class.tar.gz ~/rnaseq/data/
cd ~/rnaseq/data
tar –xvzf ibs_class.tar.gz
```

Make sure the following files really exist at `~/rnaseq/data/ibs_class`

![samples and metadata](images/samples.jpg)

Each fastq.gz file contains > 10 million reads (only 20% of the > 50 million reads per sample). Even with these small datasets, some of the analysis steps may take hours.

## 4. Quality control of raw reads, FastQC [^5]
Create a shell script called **my_fastqc.sh** in your `~/rnaseq/script` sub-directory and submit this job from `~/rnaseq/logs` sub-directory. This script is available for your convenience at [my_fastqc.sh](https://bitbucket.org/adinasarapu/ibs_class/src).

```bash
qsub ~/rnaseq/script/my_fastqc.sh
```

```bash
#!/bin/sh

# Note that all qsub directives start with '#$' while comment line starts with '#'

# name for the job.
#$ -N my_fastqc

# The cluster resources (the nodes) are grouped into Queues.
#$ -q b.q

# parallel environment (-pe) requesting 4 slots/cores/threads.
#$ -pe smp 4

# current working directory.
#$ -cwd

# merge standard error with standard output.
#$ -j y

# email options about the status of your job.
#$ -m abe
#$ -M <your_email>@emory.edu

# Directory paths as variables
PROJ_DIR=$HOME/rnaseq
SEQ_DATA_DIR=$PROJ_DIR/data/ibs_class
OUT_DIR=$PROJ_DIR/out

# Loads a package called FastQC with version: 0.11.4 
module load FastQC/0.11.4

# Creates a sub-directory for output
mkdir $OUT_DIR/QC

# runs fastqc program
fastqc -t 4 $SEQ_DATA_DIR/*.fastq.gz -o $OUT_DIR/QC

# Unloads package, FastQC (version: 0.11.4)
module unload FastQC/0.11.4
```

Type **qstat** command at terminal: 
The command for viewing job status is qstat. Without arguments, it displays status for all jobs currently in the system.

1. **qw** means waiting in the queue. This job will not run until requested resource (e.g. #$ -pe smp 4) becomes available. Once available, the job status will be changed to **r**. 
2. **r** means running

You will be notified the status of this job by email. Once this job is completed, download the results from server, `~/rnaseq/out/QC` directory. Download at least one .html file `Human_R1_SL100145_fastqc.html` and open using your favorite web browser. Based on __Per base sequence quality__ plot trim sequences using `Trimmomatic/0.36` module from HGCC (optional)!

## 5. Mapping raw reads to a reference genome, STAR [^6]
**A).** Download and uncompress Human Reference Genome Sequence data from
[https://support.illumina.com/sequencing/sequencing_software/igenome.html](https://support.illumina.com/sequencing/sequencing_software/igenome.html) using `wget`.

```bash
wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg38/Homo_sapiens_UCSC_hg38.tar.gz
```

```bash
tar --use-compress-program=pigz -xf Homo_sapiens_UCSC_hg38.tar.gz
```

___Download may take time and needs 16G disk space. It’s already available for you___ at `/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/`

**B).** Create a shell script called **my_star.sh** in your `~/rnaseq/script` sub-directory and submit this job from `~/rnaseq/logs` sub-directory. This script is available for your convenience at [my_star.sh](https://bitbucket.org/adinasarapu/ibs_class/src).

```bash
qsub ~/rnaseq/script/my_star.sh
``` 

The following script creates (a) Genome Indexing (b) Mapping reads to the genome and (c) BAM Indexing for one sample (SL100145).  Repeat STEP 2 & STEP 3 for the remaining samples (copy, paste and change Sample ID). 

```bash
#!/bin/sh
 
#$ -N my_star
#$ -q b.q
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <your_email>@emory.edu

PROJ_DIR=$HOME/rnaseq
DATA_DIR=$PROJ_DIR/data/ibs_class
OUT_DIR=$PROJ_DIR/out
 
# Reference Genome Sequence and Annotations directories
SEQUENCE_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta
ANNOTATION_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Annotation/Genes

# Load bioinformatics packages 
module load STAR/2.5.3a
module load samtools/1.5

# Create sub-directories for output
mkdir $OUT_DIR/INDEX
mkdir -p $OUT_DIR/MAPPING/{SL100145,SL100146,SL100147,SL100149,SL100150,SL100151}

# STEP 1: build STAR indexing of reference genome
STAR --runMode genomeGenerate \
 --genomeDir $OUT_DIR/INDEX \
 --genomeFastaFiles $SEQUENCE_DIR/genome.fa \
 --runThreadN 9

# STEP 2: Read alignment 
STAR --genomeDir $OUT_DIR/INDEX \
 --readFilesIn $DATA_DIR/Human_R1_SL100145.fastq.gz $DATA_DIR/Human_R2_SL100145.fastq.gz \
 --runThreadN 9 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/MAPPING/SL100145/

# STEP 3: BAM index
samtools index $OUT_DIR/MAPPING/SL100145/Aligned.sortedByCoord.out.bam

# Repeat STEP 2 & STEP 3 i.e Read alignment and BAM index for 
# the remaining samples (SL100146, SL100147, SL100149, SL100150, SL100151)

# Unload the packages
module unload samtools/1.5
module unload STAR/2.5.3a
```

## 6. Quantification of reads against genes, HTSeq [^7]
Create a shell script called **my_htseq_count.sh** in your `~/rnaseq/script` sub-directory and submit this job from `~/rnaseq/logs` sub-directory. This script is available for your convenience [my_htseq_count.sh](https://bitbucket.org/adinasarapu/ibs_class/src).

```bash
qsub ~/rnaseq/script/my_htseq_count.sh
```

```bash
#!/bin/sh
 
#$ -N my_htseq
#$ -q b.q
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <your_email>@emory.edu
  
PROJ_DIR=$HOME/rnaseq
OUT_DIR=$PROJ_DIR/out/HTSeq

# Creates a sub-directory for output
if [ ! -d $OUT_DIR ]; then
 /bin/mkdir -p $OUT_DIR
fi

# Reference Genome Annotations directory
ANNOTATION_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Annotation/Genes

# STAR aligned BAM file 
BAM_FILE_45=$PROJ_DIR/out/MAPPING/SL100145/Aligned.sortedByCoord.out.bam

# Load package
module load Anaconda2/4.2.0

# STEP 1: htseq-count
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_45 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100145.counts

# Repeat STEP 1 i.e htseq-count for the remaining samples (SL100146, SL100147, SL100149, SL100150, SL100151)

# Unload package 
module unload Anaconda2/4.2.0
```

Next step is to merge all individual counts tables (e.g SL100145.counts, SL100146.counts, SL100147.counts etc., ) into a single table. You can do this by manually using Excel (save it as `.txt` file and upload back to server). You may see a carriage-return character (`^M`) at the end of some lines in the uploaded file (to get rid of them, use `dos2unix` command or `:%s/<Ctrl-V><Ctrl-M>/\r/g` in `Vim` editor). For transferring files to and from a Linux server, use one of graphical SFTP clients.[^8] 

**OR**, Alternatively, use **qlogin** to establish a session on a compute node and combine them into one single table.

`paste`: merges files by columns

`cut`: extracts columns from the merged file

`|` pipe to connect Linux functions into one single command

`cat` sends it's output to stdout (standard output)

```bash
qlogin
cd ~/rnaseq/out/HTSeq
# run the following single line command
paste SL100145.counts SL100146.counts SL100147.counts SL100149.counts SL100150.counts SL100151.counts | cut -f1,2,4,6,8,10,12 > CountMerged.txt
# write the header to a temp-file, copy your file's contents into the temp-file after that, and move it back
echo -e "id\tSL100145\tSL100146\tSL100147\tSL100149\tSL100150\tSL100151" | cat - CountMerged.txt > tmp_count && mv tmp_count CountMerged.txt
exit
```

** Merged Table: **

 id	  | SL100145 | SL100146	| SL100147 | SL100149 | SL100150 | SL100151 
 --------|----------|----------|----------|----------|----------|----------
 A1BG	  |   38     |   50	|   43	   |   16     |   28	 |   25	    
 A1BG-AS1|   3	     |   5	|   3	   |   3      |   3	 |   3	    
 A1CF    |   0	     |   2	|   3	   |   2      |   4	 |   6	    
 A2M	  |   15     |   10	|   6	   |   8      |   19	 |   13	    
 A2M-AS1 |   5	     |   3	|   26	   |   6      |   5	 |   5	    	
 A2ML1	  |   79     |   85	|   15	   |   43     |   55	 |   68	    
 A2MP1	  |   0	     |   0	|   1	   |   0      |   1	 |   1	    
 A4GALT  |   53     |   76	|   144	   |   78     |   63	 |   57	    
 A4GNT	  |   0	     |   0	|   0	   |   1      |   0	 |   0	    
 ...	  |  ...     |  ...     |  ...     |  ...     |  ...     |  ...     

## 7. Differential expression analysis [^9]
Now using the above merged table perform a) **Gene filtering**: Keep genes with counts (> 5) in at least 3 (50%) samples. b) Perform **trimmed mean of M-values (TMM) normalization** (using edgeR, R package) and get the normalization factors. The main aim in TMM normalization is to account for library size variation between samples of interest. After filtering, it is a good idea to reset the library sizes. The library size and normalization factors are multiplied together to act as the effective library size.

To select **Differentially Expressed Genes** between two groups - apply 

**t-test**. Convert TMM normalized data to Count Per Million(CPM) and then **log2(CPM)**: Counts scaled by total number of reads (normalization for library size) necessary for comparison of expression of the same gene between samples. The t-test assumes normal distribution (within the groups). This assumption is reasonable for log(expression)-values. Then, false discovery rate (**FDR**), which is the expected fraction of false positives in a list of genes selected following a particular statistical procedure.

**edgeR**, in classic approach, tests for differential expression between two groups using a method similar in idea to the Fisher's Exact Test. This test is for a two group comparison, but if you have additional covariates, you'll need to use a generalized linear model (GLM) framework.

Create an R script called **my_diff_test.R** in your `~/rnaseq/script` sub-directory and run this job from `~/rnaseq/logs` sub-directory. This script is available for your convenience at [my_diff_test.R](https://bitbucket.org/adinasarapu/ibs_class/src).

To run an R script, use **qlogin** to establish a session on a compute node

```bash
qlogin
module load R/3.4.3 
Rscript ~/rnaseq/script/my_diff_test.R
module unload R/3.4.3
exit
```

Finally, compare **t-test** (`~/rnaseq/star_htseq_ttest_results.txt`) and **edgeR Exact test** (`~/rnaseq/star_htseq_edgeR_results.txt`) results for **HPRT1** gene!

## 8. Post Exercise [^10] 
Download raw RNA-Seq data from NCBI SRA database ([sra_wget.sh](https://bitbucket.org/adinasarapu/clustercomputing/src/eae104052129d6386caf2a896601276828b2a8db/sra_wget.sh)) for practice.

[^1]: [RNA-Seq](http://adinasarapu.github.io/files/2018_RNA_Seq_Analysis.pdf), RNA-Seq data analysis
[^2]: [I](http://adinasarapu.github.io/files/2018_Linux_Shell_I.pdf) & [II](http://adinasarapu.github.io/files/2018_Linux_Shell_II.pdf), Linux shell & shell scripting
[^3]: [HGCC - job submission](http://wingolab.org/hgcc/), Emory's Human Genetics Compute Cluster
[^4]: [HGCC - support center](https://hgcc.genetics.emory.edu), Emory's Human Genetics Compute Cluster
[^5]: [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/), A quality control tool for high throughput sequence data
[^6]: [STAR](https://github.com/alexdobin/STAR), Spliced Transcripts Alignment to a Reference
[^7]: [HTSeq](https://htseq.readthedocs.io/), Analysing high-throughput sequencing data with Python
[^8]: [FileZilla](https://filezilla-project.org) or [WinSCP](https://winscp.net/eng/index.php), SSH-enabled FTP client (SFTP client)
[^9]: [EdgeR](http://bioconductor.org/packages/release/bioc/html/edgeR.html), Empirical Analysis of Digital Gene Expression Data in R 
[^10]: [NCBI](https://www.ncbi.nlm.nih.gov/sra), Sequence Read Archive

## Note - 

**~** (tilde) is a Linux "shortcut" to denote a user's home directory.

**~** is equal to **/home/<your_user_name>** directory.

**~/** is equal to **/home/<your_user_name>/** directory.

Use correct program (**qsub**, **sh** or **Rscript**) and file path (**relative path** or **absolute path**) to run your script.

To submit a **batch job** to the HGCC queues, use the `qsub` command followed by the name of your SGE batch script file.

```bash
qsub my_fastqc.sh
qsub my_star.sh
qsub my_htseq_count.sh
```

When submitting an **interactive job**, first **qlogin** and run the script.  

```bash
Rscript my_diff_test.R
```

## References
